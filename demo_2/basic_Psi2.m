function y = basic_Psi2(x)

if x <= 0
    y = sqrt(basic_v(1 + x));
else 
    y = sqrt(basic_v(1 - x));
    
end