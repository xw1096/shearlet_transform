function output = ShearletTransform(input, psi)

input = double(input);

uST = psi.*repmat(fftshift(fft2(input)),[1,1,size(psi,3)]);
output = real(ifft2(ifftshift(ifftshift(uST,1),2)));

end