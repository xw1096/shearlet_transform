function y = basic_b(x)

if abs(x) >= 1 && abs(x) <= 2
    y = sin(pi/2*basic_v(abs(x) - 1));
elseif abs(x) > 2 && abs(x) <= 4
    y = cos(pi/2*basic_v(abs(x)/2 - 1));
else
    y = 0;
    
end