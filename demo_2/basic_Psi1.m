function y = basic_Psi1(x)

y = sqrt(basic_b(2*x)^2 + basic_b(x)^2);

end