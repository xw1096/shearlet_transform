function output = TV_SB(input, lambda)

% please make sure your input is a square image
% this is the total variance denoising base on Split Bregman:
% 
% solve min_x 1/2*|y-x|^2+lam*|Dx|
% 
% y as input noise signal
% lambda as weight function

input = double(input(:));
n = length(input);
[B, Bt, BtB] = DiffMtx(sqrt(n));
b = zeros(2*n, 1);
d = b;
output = input;
err = 1;
tol = 1e-3;
k = 1;
while err > tol && k <= 10
    tmp = output;
    [output, ~] = cgs(speye(n) + BtB, input - Bt*(b - d), 1e-5, 100);
    Bb = B*output + b;
    d = max(abs(Bb) - lambda, 0).*sign(Bb);
    b = Bb - d;
    err = norm(tmp - output)/norm(output);
    k = k + 1;
end

end