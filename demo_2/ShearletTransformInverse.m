function output = ShearletTransformInverse(input, psi)

output = sum(fftshift(fftshift(fft2(input),1),2).*psi,3);
output = real(ifft2(ifftshift(output)));
    
end