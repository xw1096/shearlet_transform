function z = basic_Phi(w1, w2)

if abs(w1) <= 1/2 && abs(w2) <= 1/2
    z = 1;
elseif abs(w1) > 1/2 && abs(w1) < 1 && abs(w2) <= abs(w1)
    z = cos(pi/2*basic_v(2*abs(w1) - 1));
elseif abs(w2) > 1/2 && abs(w2) < 1 && abs(w1) < abs(w2)
    z = cos(pi/2*basic_v(2*abs(w2) - 1));
else
    z = 0;
    
end