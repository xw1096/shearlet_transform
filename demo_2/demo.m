x = imread('lena_gray.bmp');
[m, n] = size(x);
x2 = double(x) + normrnd(0, 50, [m, n]);
psi = ShearletSpect(m, n);
y = ShearletTransform(x2, psi);
for i = 1:61
    tmp = TV_SB(y(:,:,i),7);
%     tmp = TVD_MM(y(:, :, i), 10);
    y(:, :, i) = reshape(tmp, m, n);
end
re = ShearletTransformInverse(y, psi);
figure(1)
subplot(1, 3, 1)
imagesc(x)
axis off
colormap(gray)
subplot(1, 3, 2)
imagesc(x2)
axis off
colormap(gray)
subplot(1, 3, 3)
imagesc(re)
axis off
colormap(gray)