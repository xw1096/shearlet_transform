function y = basic_v(x)

if x<0
    y = 0;
elseif x >=0 && x <= 1
    y = 35*x^4 - 84*x^5 + 70*x^6 - 20*x^7;
else
    y = 1;
end

end