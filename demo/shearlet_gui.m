function shearlet_gui

% preparation
img = imread('lena_gray.bmp');
img = double(img);
[m, n] = size(img);

% shearlet transform
psi = ShearletSpect(m, n);                % caculate shearlet spectra
%shearlet_t = ShearletTransform(img, psi); % caculate shearlet transform coefficients

% wavelet transform
filter_bank = daubechies(4);                        % caculate daubechies filter bank
%wavelet_t = WaveletTransform2(img, filter_bank, 3); % caculate wavelet transform coefficients

% edge detection
[edgeimg, ~] = edgedetect(img, 0.05);

% plotting
figure('Name', 'shearlet transform demo', 'NumberTitle', 'off')
subplot(3, 3, 1)
imagesc(img)
colormap(gray)
title('Original image')
axis off
subplot(3, 3, 2)
imagesc(img)
colormap(gray)
title('Noise image, sigma = 0.0')
axis off
subplot(3, 3, 3)
imagesc(edgeimg)
colormap(gray)
title('Original edge detection')
axis off
subplot(3, 3, 4)
imagesc(img)
colormap(gray)
title(['Shearlet transform denoise, PSNR = ' num2str(psnr(img, img))])
axis off
subplot(3, 3, 5)
imagesc(edgeimg)
colormap(gray)
title('Shearlet transform edge detection')
axis off
subplot(3, 3, 7)
imagesc(img)
colormap(gray)
title(['Wavelet transform denoise, PSNR = ' num2str(psnr(img, img))])
axis off
subplot(3, 3, 8)
imagesc(edgeimg)
colormap(gray)
title('Wavelet transform edge detection')
axis off

drawnow;

slider = uicontrol('Style', 'slider', ...
    'Tag', 'slider1',...
    'Min', 10, 'Max', 50,...
    'Value', 10, ...
    'SliderStep', [0.025 0.025], ...
    'Position', [5 5 200 20], ...          
    'Callback',  {@fun1, img, m, n, psi, filter_bank}    );

txt = uicontrol('Style','text',...
        'Position',[205 5 200 20],...
        'String','Change noisy deviation here');
    

end

function fun1(hObject, ~, img, m, n, psi, filter_bank)

sigma = get(hObject, 'Value');
img_noise = img + normrnd(0, sigma, [m, n]);

% shearlet transform denoise
shearlet_t = ShearletTransform(img_noise, psi); % caculate shearlet transform coefficients
sigma_shearlet = ShearletTransformNoiseSigma(m, n, sigma, psi);
for i = 1:size(psi, 3)
    T = sqrt(2)*sigma_shearlet(i)^2/sqrt(var(reshape(shearlet_t(:, :, i), m*n, 1))-sigma_shearlet(i)^2);
    if ~isreal(T)
        T = max(max(shearlet_t(:, :, i)));
    end
    shearlet_t(:, :, i) = soft_thresh(shearlet_t(:, :, i), T);
    
end
shearlet_re = ShearletTransformInverse(shearlet_t, psi);
%shearlet_re = test(shearlet_re,1,10,50);
[shearlet_edg, ~] = edgedetect(shearlet_re, 0.05);

% wavelet transform denoise
wavelet_t = WaveletTransform2(img_noise, filter_bank, 3); % caculate wavelet transform coefficients
for i = 1:length(wavelet_t)
    T = sigma^2*sqrt(2)/sqrt(var(reshape(wavelet_t(i).data,[1,length(wavelet_t(i).data)^2]))-sigma^2);
    if ~isreal(T)
        T = max(max(wavelet_t(i).data));
    end
    wavelet_t(i).data = soft_thresh(wavelet_t(i).data, T);
end
wavelet_re = WaveletTransformInv2(wavelet_t, filter_bank);
[wavelet_edg, ~] = edgedetect(wavelet_re, 0.05);

% plotting
subplot(3, 3, 2)
imagesc(img_noise)
colormap(gray)
title(['Noise image, sigma = ' num2str(sigma)])
axis off
subplot(3, 3, 4)
imagesc(shearlet_re)
colormap(gray)
title(['Shearlet transform denoise, PSNR = ' num2str(psnr(shearlet_re, img))])
axis off
subplot(3, 3, 5)
imagesc(shearlet_edg)
colormap(gray)
title('Shearlet transform edge detection')
axis off
subplot(3, 3, 7)
imagesc(wavelet_re)
colormap(gray)
title(['Wavelet transform denoise, PSNR = ' num2str(psnr(wavelet_re, img))])
axis off
subplot(3, 3, 8)
imagesc(wavelet_edg)
colormap(gray)
title('Wavelet transform edge detection')
axis off

end
