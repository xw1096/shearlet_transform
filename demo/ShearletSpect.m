function psi = ShearletSpect(m, n)

j0 = floor(1/2*log2(max(m, n)));
w1 = floor(-n/2):ceil(n/2)-1;
w2 = floor(-m/2):ceil(m/2)-1;
psi = zeros(m, n, 2^(j0+2) - 3);

tmp = zeros(m, n);
% low frequency domain, built matrix
for i = 1:n
    for j = 1:m
        tmp(i, j) = basic_Phi(w1(i), w2(j));
    end
end
psi(:, :, 1) = tmp;

index = 2;
tmp_h = zeros(m, n);
tmp_v = zeros(m, n);
tmp_a = zeros(m, n);
for j = 0:j0-1
    for k = 0:-1:1 - 2^j
        for x = 1:n
            for y = 1:m
                if abs(4^(-j)*w1(x)) >= 1/2 && abs(4^(-j)*w1(x)) > abs(4^(-j)*k*w1(x) + 2^(-j)*w2(y))
                    tmp_h(y, x) = basic_Psi(4^(-j)*w1(x), 4^(-j)*k*w1(x) + 2^(-j)*w2(y));
                else
                    tmp_h(y, x) = 0;
                end
            end
        end
        psi(:, :, index) = tmp_h;
        index = index + 1;  
    end
    for x = 1:n
        for y = 1:m
            if abs(4^(-j)*w1(x)) >= 1/2 && abs(4^(-j)*w1(x)) > abs(4^(-j)*(-2^j)*w1(x) + 2^(-j)*w2(y))
                tmp_a(y, x) = basic_Psi(4^(-j)*w1(x), 4^(-j)*(-2^j)*w1(x) + 2^(-j)*w2(y)) + tmp_a(y, x);
            elseif abs(4^(-j)*w2(y)) >= 1/2 && abs(4^(-j)*w2(y)) > abs(4^(-j)*(-2^j)*w2(y) + 2^(-j)*w1(x))
                tmp_a(y, x) = tmp_a(y,x) + basic_Psi(4^(-j)*w2(y), 4^(-j)*(-2^j)*w2(y) + 2^(-j)*w1(x));
            end
        end
    end 
    psi(:, :, index) = tmp_a;
    tmp_a = zeros(m, n);
    index = index + 1;
    for k = 1-2^j:2^j-1
        for x = 1:n
            for y = 1:m
                if abs(4^(-j)*w2(y)) >= 1/2 && abs(4^(-j)*w2(y)) > abs(4^(-j)*k*w2(y) + 2^(-j)*w1(x))
                %tmp_h(x, y) = basic_Psi(4^(-j)*w1(x), 4^(-j)*k*w1(x) + 2^(-j)*w2(y));
                    tmp_v(y, x) = basic_Psi(4^(-j)*w2(y), 4^(-j)*k*w2(y) + 2^(-j)*w1(x));
                else
                    tmp_v(y, x) = 0;
                end
            end
        end
        psi(:, :, index) = tmp_v;
        index = index + 1;
    end 
    for x = 1:n
        for y = 1:m
            if abs(4^(-j)*w1(x)) >= 1/2 && abs(4^(-j)*w1(x)) > abs(4^(-j)*(2^j)*w1(x) + 2^(-j)*w2(y))
                tmp_a(y, x) = basic_Psi(4^(-j)*w1(x), 4^(-j)*(2^j)*w1(x) + 2^(-j)*w2(y)) + tmp_a(y, x);
            elseif abs(4^(-j)*w2(y)) >= 1/2 && abs(4^(-j)*w2(y)) > abs(4^(-j)*(2^j)*w2(y) + 2^(-j)*w1(x))
                tmp_a(y, x) = tmp_a(y,x) + basic_Psi(4^(-j)*w2(y), 4^(-j)*(2^j)*w2(y) + 2^(-j)*w1(x));
            end
        end
    end 
    psi(:, :, index) = tmp_a;
    tmp_a = zeros(m, n);
    index = index + 1;
    for k = 2^j - 1:-1:1
        for x = 1:n
            for y = 1:m
                if abs(4^(-j)*w1(x)) >= 1/2 && abs(4^(-j)*w1(x)) > abs(4^(-j)*k*w1(x) + 2^(-j)*w2(y))
                    tmp_h(y, x) = basic_Psi(4^(-j)*w1(x), 4^(-j)*k*w1(x) + 2^(-j)*w2(y));
                else
                    tmp_h(y, x) = 0;
                end
            end
        end
        psi(:, :, index) = tmp_h;
        index = index + 1;
    end
end
end