function output = ShearletTransformNoiseSigma(m, n, sigma, psi)

output = zeros(1, size(psi, 3));

noise = normrnd(0, sigma, [m, n]);
uST = psi.*repmat(fftshift(fft2(noise)),[1,1,size(psi,3)]);
tmp = real(ifft2(ifftshift(ifftshift(uST,1),2)));
for j = 1:size(psi, 3)
    output(1, j) = std(reshape(tmp(:, :, j), m*n, 1));
end
