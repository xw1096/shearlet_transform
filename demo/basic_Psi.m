function z = basic_Psi(w1, w2)

z = basic_Psi1(w1)*basic_Psi2(w2/w1);

end