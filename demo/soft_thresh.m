function output = soft_thresh(input, T)
[m, n] = size(input);
output = zeros(m, n);
for i = 1:m
    for j = 1:n
        if abs(input(i, j)) <= T
            output(i, j) = 0;
        else
            output(i, j) = sign(input(i, j)) * (abs(input(i, j)) - T);
        end
    end
end
end