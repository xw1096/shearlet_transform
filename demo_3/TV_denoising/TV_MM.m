function x = TV_MM(y, lam)

% this is the total variance denoising base on Majorization Minimization:
% 
% solve min_x 1/2*|y-x|^2+lam*|Dx|
% 
% y as input noise signal
% lam as weight function


y = double(y);
%[m, n] = size(y);
y = y(:);
n = length(y);
x = y;
[~, ~, D] = DiffMtx(sqrt(n));
err = 1;
tol = 1e-3;
K = 1;
while err >= tol && K<=100 
    x_old = x;
    [tmp, ~] = cgs(1/lam*spdiags(abs(D*x), 0, n, n) + D*D', D*y, 1e-5, 100); 
    x = y - D'*tmp;
    err = norm(x_old - x)/norm(x);
    K = K+1;
end

%x = reshape(x, m, n);

end