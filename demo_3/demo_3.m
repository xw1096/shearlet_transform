%% 2-D Dual-Tree Wavelet Transform based Image Denoising
% In this demo, MM (Majorization-Minimization) and SB (Split Bregman) will
% be used to do image denoising based on 2-D Dual-Tree Wavelet Transform,
% for more information, see paper:
% <http://dsp.vscht.cz/musoko/pdf/pc04.pdf COMPLEX WAVELET TRANSFORM IN SIGNAL AND IMAGE ANALYSIS>

%% Complex Wavelet Transform Filter Bank
% The generation of Complex Wavelet Transform Filter Bank is based on paper:
% <http://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=899397 A DUAL-TREE COMPLEX WAVELET TRANSFORM WITH IMPROVED ORTHOGONALITY AND SYMMETRY PROPERTIES>
% , here a length 10 filter  bank will be used

Faf{1} = [
                  0                  0
  -0.08838834764832  -0.01122679215254
   0.08838834764832   0.01122679215254
   0.69587998903400   0.08838834764832
   0.69587998903400   0.08838834764832
   0.08838834764832  -0.69587998903400
  -0.08838834764832   0.69587998903400
   0.01122679215254  -0.08838834764832
   0.01122679215254  -0.08838834764832
                  0                  0
 ];
   
Fsf{1} = Faf{1}(end:-1:1, :);

Faf{2} = [
   0.01122679215254                  0
   0.01122679215254                  0
  -0.08838834764832  -0.08838834764832
   0.08838834764832  -0.08838834764832
   0.69587998903400   0.69587998903400
   0.69587998903400  -0.69587998903400
   0.08838834764832   0.08838834764832
  -0.08838834764832   0.08838834764832
                  0   0.01122679215254
                  0  -0.01122679215254
];

Fsf{2} = Faf{2}(end:-1:1, :);

af{1} = [
   0.03516384000000                  0
                  0                  0
  -0.08832942000000  -0.11430184000000
   0.23389032000000                  0
   0.76027237000000   0.58751830000000
   0.58751830000000  -0.76027237000000
                  0   0.23389032000000
  -0.11430184000000   0.08832942000000
                  0                  0
                  0  -0.03516384000000
 ];
 
af{2} = [
                  0  -0.03516384000000
                  0                  0
  -0.11430184000000   0.08832942000000
                  0   0.23389032000000
   0.58751830000000  -0.76027237000000
   0.76027237000000   0.58751830000000
   0.23389032000000                  0
  -0.08832942000000  -0.11430184000000
                  0                  0
   0.03516384000000                  0
];

sf{1} = af{1}(end:-1:1, :);
 
sf{2} = af{2}(end:-1:1, :);

Obj = {' low', ' high'};
 figure('position', [0, 0, 700, 500])
title('analysis filter frequency response')
xlim([0 pi])
hold on
for j = 1:2
    for i = 1:2
        [h, w] = freqz(Faf{j}(:, i), 1);
        plot(w, abs(h), 'DisplayName', ['first level  tree ' num2str(j) Obj{i} ' pass filter']);
    end
end

for j = 1:2
    for i = 1:2
        [h, w] = freqz(af{j}(:, i), 1);
        plot(w, abs(h), 'DisplayName', ['rest level  tree ' num2str(j) Obj{i} ' pass filter']);
    end
end
legend('show')

%% Noise Image Complex Wavelet Transform Subbands
% The dual-tree complex DWT of a signal x is implemented using two
% critically-sampled DWTs in parallel on the same data, as shown in the
% figure.
% 
% <<fig71.jpg>>
%

%%
img = imread('lena_gray.bmp');
[m, n] = size(img);
img_noise = double(img) + normrnd(0, 50, [m, n]);

figure('position', [0 0 1000 400])
subplot(1, 2, 1)
imagesc(img)
colormap(gray)
axis off
title('original image')
subplot(1, 2, 2)
imagesc(img_noise)
colormap(gray)
axis off
title(['noise image psnr=' num2str(psnr(img_noise, double(img)))])
%%
% original image level 3 subbands
%%
WTsubband = cplxdual2D(double(img), 5, Faf, af);

for i = 1:2
    for j = 1:2
        for k = 1:3
            subplot(2, 6, (i-1)*6+(j-1)*3+k)
            imagesc(WTsubband{3}{i}{j}{k})
            colormap(gray)
            axis off
        end
    end
end
%%
% nosie image level 3 subbands
%%
WTsubband = cplxdual2D(img_noise, 5, Faf, af);

for i = 1:2
    for j = 1:2
        for k = 1:3
            subplot(2, 6, (i-1)*6+(j-1)*3+k)
            imagesc(WTsubband{3}{i}{j}{k})
            colormap(gray)
            axis off
        end
    end
end

%% Image denoising use Total Variation
% Here two algorithms used to solve $argmin_x{F(x)=\frac{1}{2}\sum_{n=0}^{N-1}|y(n)-x(n)|^2+\lambda\sum_{n=1}^{N-1}|x(n)-x(n-1)|}$
%%% <http://eeweb.poly.edu/iselesni/lecture_notes/TVDmm/TVDmm.pdf Majorization Minimization>
I = sqrt(-1);
for j = 1:5
    for s1 = 1:2
        for s2 = 1:3
            tmp = WTsubband{j}{1}{s1}{s2} + I*WTsubband{j}{2}{s1}{s2};
            [tmp_m, tmp_n] = size(tmp);
            tmp = TV_MM(tmp, 20);
            tmp = reshape(tmp, tmp_m, tmp_n);
            WTsubband{j}{1}{s1}{s2} = real(tmp);
            WTsubband{j}{2}{s1}{s2} = imag(tmp);
        end
    end
end
figure('position', [0 0 500 500])
denoise = icplxdual2D(WTsubband, 5, Fsf, sf);
imagesc(denoise)
colormap(gray)
axis off
title(['MM denoising image psnr=' num2str(psnr(denoise, double(img)))])

%%% <ftp://ftp.math.ucla.edu/pub/camreport/cam08-29.pdf SPLIT BREGMAN METHOD>
WTsubband = cplxdual2D(img_noise, 5, Faf, af);

for j = 1:5
    for s1 = 1:2
        for s2 = 1:3
            tmp = WTsubband{j}{1}{s1}{s2} + I*WTsubband{j}{2}{s1}{s2};
            [tmp_m, tmp_n] = size(tmp);
            tmp = TV_SB(tmp, 20);
            tmp = reshape(tmp, tmp_m, tmp_n);
            WTsubband{j}{1}{s1}{s2} = real(tmp);
            WTsubband{j}{2}{s1}{s2} = imag(tmp);
        end
    end
end
figure('position', [0 0 500 500])
denoise = icplxdual2D(WTsubband, 5, Fsf, sf);
imagesc(denoise)
colormap(gray)
axis off
title(['SB denoising image psnr=' num2str(psnr(denoise, double(img)))])
