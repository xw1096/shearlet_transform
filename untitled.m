x = imread('Documents/shearlet_transform/demo/lena_gray.bmp');
[m, n] = size(x);
x2 = double(x) + normrnd(0, 50, [m, n]);
psi = ShearletSpect(m, n);
y = ShearletTransform(x2, psi);
for i = 1:61
    tmp = TV_SB(y(:,:,i),7);
%     tmp = TVD2(y(:, :, i), 10);
    y(:, :, i) = reshape(tmp, m, n);
end
re = ShearletTransformInverse(y, psi);
figure(1)
imagesc(re)
colormap(gray)
figure(2)
imagesc(x2)
colormap(gray)