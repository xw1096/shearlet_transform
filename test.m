function output  = test(input, lambda, Nit)

output = 0*input;
alpha = 0.5;

for i = 1:Nit
    output = (alpha/(alpha + lambda))*output + (1/(alpha + lambda))*convt2(1, (input - conv2(1, output))); 
end

end